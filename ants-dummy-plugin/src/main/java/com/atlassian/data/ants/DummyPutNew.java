package com.atlassian.data.ants;

import com.atlassian.data.ants.api.Ants;
import com.atlassian.data.ants.api.Entry;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;

import javax.inject.Inject;
import javax.inject.Named;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Named
public class DummyPutNew extends HttpServlet
{
    private final Ants ants;

    @Inject
    public DummyPutNew(@SuppressWarnings("SpringJavaAutowiringInspection") @ComponentImport final Ants ants)
    {
        this.ants = ants;
    }

    @Override
    protected void doGet(final HttpServletRequest req, final HttpServletResponse resp)
            throws ServletException, IOException
    {
        Entry<String> entry = ants.putString("myKey", "myValue");

        resp.setContentType("text/plain");
        resp.getWriter().write("put: " + entry.value() + " " + entry.version() + " " + entry.date() + "\n");
    }
}
