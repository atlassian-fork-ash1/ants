package com.atlassian.data.ants.rdbms.service;

import com.atlassian.data.ants.api.Ants;
import com.atlassian.data.ants.api.Entry;
import com.atlassian.data.ants.rdbms.db.Bootstrap;
import com.atlassian.data.ants.rdbms.provider.StringProvider;
import com.atlassian.plugin.spring.scanner.annotation.export.ExportAsService;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.atlassian.sal.api.lifecycle.LifecycleAware;
import com.atlassian.sal.api.rdbms.TransactionalExecutorFactory;
import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;

import java.sql.Connection;
import java.util.function.Function;
import javax.annotation.Nonnull;
import javax.inject.Inject;
import javax.inject.Named;

import static com.google.common.base.Preconditions.checkNotNull;

@SuppressWarnings ("SpringJavaAutowiringInspection")
@ExportAsService ({ Ants.class, LifecycleAware.class })
@Named ("ants")
public class AntsService implements Ants, LifecycleAware
{
    private final TransactionalExecutorFactory transactionalExecutorFactory;

    // TODO: factory to create this class instead of exporting it
    private final String id = "123456";

    private final LoadingCache<String, StringProvider> stringProviders = CacheBuilder.newBuilder().build(new CacheLoader<String, StringProvider>()
    {
        @Override
        public StringProvider load(@Nonnull final String id) throws Exception
        {
            return new StringProvider(id);
        }
    });

    @Inject
    public AntsService(@Nonnull @ComponentImport final TransactionalExecutorFactory transactionalExecutorFactory)
    {
        checkNotNull(transactionalExecutorFactory);
        this.transactionalExecutorFactory = transactionalExecutorFactory;
    }

    @Override
    public void onStart()
    {
        transactionalExecutorFactory.createExecutor(true, true).execute(new Function<Connection, Void>()
        {
            @Override
            public Void apply(final Connection connection)
            {
                Bootstrap.execute(connection, id);
                return null;
            }
        });
    }

    @Override
    public Entry<String> getString(@Nonnull final String key)
    {
        checkNotNull(key);
        return transactionalExecutorFactory.createExecutor(true, true).execute(new Function<Connection, Entry<String>>()
        {
            @Override
            public Entry<String> apply(final Connection connection)
            {
                return stringProviders.getUnchecked(id).get(connection, key);
            }
        });
    }

    @Override
    public Entry<String> getString(@Nonnull final String key, final int version)
    {
        checkNotNull(key);
        return transactionalExecutorFactory.createExecutor(true, true).execute(new Function<Connection, Entry<String>>()
        {
            @Override
            public Entry<String> apply(final Connection connection)
            {
                return stringProviders.getUnchecked(id).get(connection, key, version);
            }
        });
    }

    @Override
    public Entry<String> putString(@Nonnull final String key, final String value)
    {
        checkNotNull(key);
        return transactionalExecutorFactory.createExecutor(true, true).execute(new Function<Connection, Entry<String>>()
        {
            @Override
            public Entry<String> apply(final Connection connection)
            {
                return stringProviders.getUnchecked(id).put(connection, key, value);
            }
        });
    }

    @Override
    public Entry<String> putString(@Nonnull final String key, final String value, final int currentVersion)
    {
        checkNotNull(key);
        return transactionalExecutorFactory.createExecutor(true, true).execute(new Function<Connection, Entry<String>>()
        {
            @Override
            public Entry<String> apply(final Connection connection)
            {
                return stringProviders.getUnchecked(id).put(connection, key, value, currentVersion);
            }
        });
    }
}
